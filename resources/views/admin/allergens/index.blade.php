<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 22/04/2021
 * Time: 12:01
 */
?>

@extends('layouts.admin')

@section('breadcrumb')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $titlePage }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Página Principal</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('contenido')

    <div class="row">

        <div class="col-lg-12">

            <div class="panel-body">

                @if(Session::has('info'))
                    <div class="alert alert-info" role="alert">
                        <strong>{{ Session::get('info') }}</strong>
                    </div>
                @endif

                @if($allergens->isEmpty())
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="fas fa-exclamation-triangle"></i> ¡Atención!</h4>
                        No existen datos que mostrar
                    </div>
                    {{--<div class="row">--}}
                        {{--<div class="col-lg-12">--}}
                            {{--<a href="{{ route('allergens.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo alérgeno</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <br>

                @else

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">

                                <div class="card-header">

                                    <h3 class="card-title">
                                        Listado de Alérgenos
                                    </h3>

                                    {{--<div>--}}
                                        {{--<a href="{{ route('allergens.create') }}" class="btn btn-warning float-right"><i class="fa fa-plus"></i> Nuevo alérgeno</a>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="card-body">

                                    <form action="{{ route('allergens.index') }}" method="GET" role="search" autocomplete="off">
                                        @csrf
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="term" placeholder="Introduzca el nombre de un alérgeno" id="term">

                                            <div class="input-group-btn mr-3">
                                                <button class="btn btn-outline-dark" type="submit" title="Buscar alérgenos">
                                                    <span class="fas fa-search"></span>
                                                </button>
                                            </div>
                                            <a href="{{ route('allergens.index') }}">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-outline-success" type="button" title="Actualizar página">
                                                       <span class="fas fa-sync-alt"></span>
                                                    </button>
                                                    </span>
                                            </a>
                                        </div>
                                    </form>
                                    <br>

                                    {{--<div class="table-responsive">--}}

                                    {{--<table class="table table-striped table-bordered table-hover">--}}
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Imagen</th>
                                            {{--<th colspan="2">Acciones</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allergens as $allergen)
                                            <tr>
                                                <td>{{ $allergen->id }}</td>
                                                <td>{{ $allergen->name }}</td>
                                                {{--<td width="60%">{{ $allergen->description }}</td>--}}
                                                <td width="70%">{{ $allergen->description }}</td>
                                                <td>@if($allergen->image)
                                                        <img id="picture" src="{{ Storage::url($allergen->image->url) }}" alt="{{ $allergen->image->alternativeText }}" width="50%">
                                                    @else
                                                        <img id="picture" src="https://cdn.pixabay.com/photo/2015/03/25/13/04/page-not-found-688965_960_720.png" alt="No has cargado ninguna imagen" width="20%">
                                                    @endif
                                                </td>
                                                {{--<td width="10px">--}}
                                                    {{--<a class="btn btn-secondary btn-sm" href="{{ route('allergens.edit', $allergen) }}" title="Editar"><i--}}
                                                                {{--class="fas fa-pen"></i></a>--}}
                                                {{--</td>--}}
                                                {{--<td width="10px">--}}
                                                    {{--<a class="btn btn-danger btn-sm" title="Eliminar" data-toggle="modal"--}}
                                                       {{--data-target="#modalEliminar" data-href="{{ route('allergens.destroy', $allergen) }}"--}}
                                                       {{--href='#'><i class='fa fa-trash'></i></a>--}}

                                                {{--</td>--}}
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            {{--<th colspan="3">Acciones</th>--}}
                                        </tr>
                                        </tfoot>
                                    </table>

                                    {{--</div>--}}

                                </div>

                                <div class="card-footer d-flex justify-content-center">

                                    {{ $allergens->links() }}

                                </div>

                            </div>

                        </div>

                    </div>
                @endif

            </div>

        </div>
    </div>

@endsection


@section('modalEliminar')

    <!-- Dialogo modal eliminar -->

    <div class="modal modal-danger fade" id="modalEliminar" name="modalEliminar" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-danger">
                <div class="modal-header" id="titl">
                    <h4 class="modal-title" id="titleModalEliminar">¡Eliminar Alérgeno!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h5 style="font-family:Verdana; font-size: 14px;" id="bodyModalEliminar" class="debug-url">
                        ¿Está seguro de que desea eliminar el alérgeno?
                    </h5>
                    <form action="" class="form-modal" method="POST">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-outline-light btn-ok">Eliminar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    {{--<!-- DataTables -->--}}
    {{--<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>--}}
    {{--<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>--}}

    {{--<!-- page script -->--}}
    {{--<script>--}}
    {{--$(function () {--}}
    {{--$('#categories').DataTable({});--}}
    {{--});--}}
    {{--</script>--}}

    <!-- Modal Eliminar -->
    <script>
        $('#modalEliminar').on('show.bs.modal', function (e) {
            $(this).find('.form-modal').attr('action', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').click(function () {
                $("form").submit();
            });
        });
    </script>

@endsection