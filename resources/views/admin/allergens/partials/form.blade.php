<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 20:22
 */
?>

<div class="card-body">
    <div class="form-group">
        {{--<label for="name">Nombre</label>--}}
        {!! Form::label('name', 'Nombre') !!}

        {{--<input type="text" class="form-control @error('name') is-invalid @enderror" id="name"--}}
               {{--name="name" placeholder="Nombre" value="{{ old('name') }}" required>--}}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Introduce el nombre del alérgeno', 'required' ]) !!}

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        {{--<label for="description">Descripción</label>--}}
        {!! Form::label('description', 'Descripción') !!}

        {{--<input type="text" class="form-control @error('description') is-invalid @enderror" id="description"--}}
               {{--name="description"--}}
               {{--placeholder="Descripción" value="{{ old('description') }}" required>--}}
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Introduce la descripción del alérgeno', 'required' ]) !!}

        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="row mb-3">
        <div class="col">
            <div class="image-wrapper">
                @isset ($allergen->image) <!-- si existe una imagen relacionada con este alérgeno -->
                    <img id="picture" src="{{ Storage::url($allergen->image->url) }}">
                @else
                    <img id="picture" src="https://cdn.pixabay.com/photo/2015/03/25/13/04/page-not-found-688965_960_720.png" alt="No has cargado ninguna imagen">
                @endisset
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                {{--<label for="file">Imagen del alérgeno</label>--}}
                {!! Form::label('file', 'Imagen del alérgeno') !!}

                {{--<input type="file" id="file" name="file" accept="image/png, image/jpeg" class="form-control-file">--}}
                {!! Form::file('file', ['class' => 'form-control-file', 'accept' => 'image/png, image/jpeg', 'required']) !!}
                {{--{!! Form::file('file', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}--}}

                @error('file')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>
            <p>NOTA: Las imágenes deberán tener un tamaño de 57 x 57 píxeles</p>
            <!-- Tener en cuenta que el campo texto alternativo se rellenará con mismo texto que se incluya en descripción pero se puede editar (acortar) -->
            <div class="form-group">
                {{--<label for="alternativeText">Texto alternativo: </label>--}}
                {!! Form::label('alternativeText', 'Texto alternativo:') !!}

                {{--<input type="text" class="form-control @error('alternativeText') is-invalid @enderror" id="alternativeText"--}}
                       {{--name="alternativeText">--}}
                {!! Form::text('alternativeText', null, ['class' => 'form-control', 'placeholder' => 'Introduce un texto alternativo para describir la imagen', 'required']) !!}


                @error('alternativeText')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>

        </div>

    </div>

</div>
<!-- /.card-body -->


@section('css')
    @parent

    <style>
        .image-wrapper {
            position:relative;
            padding-bottom: 25%;
        }

        .image-wrapper img {
            position: absolute;
            object-fit: scale-down;
            width: 100%;
            height: 100%;
        }
    </style>
@endsection

@section('scripts')
    @parent

    <script>
        //Cambiar imagen --> para previsualizar la imagen cargada en el formulario de creación/edición del post
        //esta a la escucha de cualquier cambio que hagamos a nuestro input File
        document.getElementById("file").addEventListener('change', cambiarImagen);

        function cambiarImagen(event){
            var file = event.target.files[0];

            var reader = new FileReader();
            //cambia la imagen seleccionada en el elemento con id "picture" para previsualizarla
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };
            reader.readAsDataURL(file);
        }
    </script>

    <script>
        // Campo Descripción y Texto alternativo de la imagen
        $(document).ready(function () {
            $("#description").keyup(function () {
                var value = $(this).val();
                $("#alternativeText").val(value);
            });
        });
    </script>

@endsection
