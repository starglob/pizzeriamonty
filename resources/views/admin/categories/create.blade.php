<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 22/04/2021
 * Time: 12:38
 */
?>
@extends('layouts.admin')

@section('breadcrumb')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $titlePage }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Página Principal</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('contenido')
    <div class="card">

        {!! Form::open(['route' => 'categories.store', 'autocomplete' => 'off']) !!}

        @include('admin.categories.partials.form')

        <div class="card-footer">
            {{--<button type="submit" class="btn btn-primary">{{ __('Crear Alérgeno') }}</button>--}}
            {!! Form::submit('Crear Categoría', ['class' => 'btn btn-primary'])!!}

            <a class="btn btn-link" href="{{ url()->previous() }}">{{ __('Volver') }}</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
