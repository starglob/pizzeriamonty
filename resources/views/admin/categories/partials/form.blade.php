<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 20:22
 */
?>

<div class="card-body">
    <div class="form-group">
        {!! Form::label('name', 'Nombre') !!}

        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Introduce el nombre de la categoría', 'required' ]) !!}

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Descripción (opcional)') !!}

        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Introduce una descripción de la categoría' ]) !!}

        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

</div>
<!-- /.card-body -->

