<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 23/06/2021
 * Time: 11:50
 */
?>

        <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{-- $titlePage --}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

@section('css')
    <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('adminZone/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{ asset('adminZone/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


        <!-- Estilos personalizados -->
        <link rel="stylesheet" href="{{ asset('stylesheets/custom.css') }}">

    @show
</head>
<body onload="window.print();">

<div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
{{--<div class="content-wrapper">--}}
<!-- Main content -->
    <section class="content">

        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header oculto-impresion">
                            <h3 class="card-title">Menú degustación</h3>
                            <div class="card-tools">
                                <a href="{{ route("admin") }}" class="oculto-impresion float-right" style="color: #ff0000; font-size: 20px; "><b>VOLVER</b></a>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (!isset($menuDegustacion))
                                <p class="m-6 text-center text-danger"> No hay menús de degustación creados</p>
                            @else

                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <h5 style="float: right;">{{ date('d/m/Y', strtotime($menuDegustacion->dateMenu)) }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 text-center mb-0">
                                        <img src="{{ asset("assets/pizzeria-monty/images/MenuDegustacion.svg") }}" alt="Menú degustación">
                                    </div>
                                </div>
                                <br/>

                                <!-- Obteniendo las categorías asociadas al Menú degustación -->
                                @for ($j=0; $j < $numCategoriesMD; $j++)

                                    <div class="mt-0">
                                        <p><h4 class="text-center" style="color: #FE7A13;">{{ $categoriesMD[$j]->name }}</h4></p>
                                    </div>
                                    @foreach($dishes as $dish)
                                        @foreach($dishes_menuD as $dmDeg)
                                            @if (($dmDeg->dish_id == $dish->id) and ($dmDeg->category_id == $categoriesMD[$j]->id))
                                                <div>
                                                    <p><h5 class="text-center ">{{ $dish->name }} @foreach($dish->allergens as $dish->allergen) <img src="{{ Storage::url($dish->allergen->image->url) }}" alt="alergenos" width="25px;"> @endforeach</h5></p>
                                                    <p><h6 class="text-center mb-4">@if($dish->ingredients) Contiene: {{ $dish->ingredients }}@endif</h6></p>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach

                                @endfor


                                <h5 class="mt-4 mb-3" style="margin-left: 5%;">{{ $menuDegustacion->description }}</h5>
                                <h1 class="text-center">{{ $menuDegustacion->price }} €</h1>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

</body>

</html>
