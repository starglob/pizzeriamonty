<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 22/04/2021
 * Time: 12:01
 */
?>
@extends('layouts.admin')

@section('breadcrumb')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $titlePage }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Página Principal</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('contenido')

    <div class="row">

        <div class="col-lg-12">

            <div class="panel-body">

                @if(Session::has('info'))
                    <div class="alert alert-info" role="alert">
                        <strong>{{ Session::get('info') }}</strong>
                    </div>
                @endif

                @if($menus->isEmpty())
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="fas fa-exclamation-triangle"></i> ¡Atención!</h4>
                        No existen datos que mostrar
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('menus.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo menú</a>
                        </div>
                    </div>
                    <br>

                @else

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">

                                <div class="card-header">

                                    <h3 class="card-title">
                                        Listado de Menús
                                    </h3>

                                    <div>
                                        <a href="{{ route('menus.create') }}" class="btn btn-warning float-right"><i class="fa fa-plus"></i> Nuevo menú</a>
                                    </div>
                                </div>

                                <div class="card-body">

                                    <form action="{{ route('menus.index') }}" method="GET" role="search" autocomplete="off">
                                        @csrf
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="term" placeholder="Introduzca el nombre de un plato" id="term">

                                            <div class="input-group-btn mr-3">
                                                <button class="btn btn-outline-dark" type="submit" title="Buscar platos">
                                                    <span class="fas fa-search"></span>
                                                </button>
                                            </div>
                                            <a href="{{ route('menus.index') }}">
                                                    <span class="input-group-btn">
                                                    <button class="btn btn-outline-success" type="button" title="Actualizar página">
                                                       <span class="fas fa-sync-alt"></span>
                                                    </button>
                                                    </span>
                                            </a>
                                        </div>
                                    </form>
                                    <br>

                                    {{--<div class="table-responsive">--}}

                                    {{--<table class="table table-striped table-bordered table-hover">--}}
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tipo de menú</th>
                                            <th>Categorías</th>
                                            <th>Platos</th>
                                            <th>Descripción</th>
                                            <th>Precio</th>
                                            <th>Fecha</th>
                                            <th colspan="2">Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($menus as $menu)
                                            <tr>
                                                <td>{{ $menu->id }}</td>
                                                <td>{{ $menu->type->name }}</td>
                                                <td>@php $name= " "; foreach($menu->categories as $category){ if($category->name <> $name) echo $category->name; echo "<br>";   $name=$category->name;} @endphp  </td>
                                                <td>@foreach($menu->dishes as $dish)  {{ $dish->name }} <br> @endforeach</td>

                                                <td style="width: 15%;">{{ $menu->description }}</td>
                                                <td>{{ $menu->price }}</td>
                                                {{--<td>{{ date('d/m/Y', strtotime($menu->dateMenu)) }}</td>--}}
                                                <td>@if($menu->dateMenu) {{ date('d/m/Y', strtotime($menu->dateMenu)) }} @endif </td>
                                                {{--<td width="10px">--}}
                                                    {{--<a class="btn btn-primary btn-sm" href="{{ route('menus.show', $menu) }}"><i--}}
                                                    {{--class="fas fa-eye"></i></a>--}}
                                                {{--</td>--}}
                                                <td width="10px">
                                                    <a class="btn btn-secondary btn-sm" href="{{ route('menus.edit', $menu->id) }}" title="Editar"><i
                                                                class="fas fa-pen"></i></a>
                                                </td>
                                                <td width="10px">
                                                    <a class="btn btn-danger btn-sm" title="Eliminar" data-toggle="modal"
                                                       data-target="#modalEliminar" data-href="{{ route('menus.destroy', $menu->id) }}"
                                                       href='#'><i class='fa fa-trash'></i></a>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tipo de menú</th>
                                            <th>Categorías</th>
                                            <th>Platos</th>
                                            <th>Descripción</th>
                                            <th>Precio</th>
                                            <th>Fecha</th>
                                            <th colspan="2">Acciones</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    {{--</div>--}}

                                </div>

                                <div class="card-footer d-flex justify-content-center">

                                    {{ $menus->links() }}

                                </div>

                            </div>

                        </div>

                    </div>
                @endif

            </div>

        </div>
    </div>

@endsection


@section('modalEliminar')

    <!-- Dialogo modal eliminar -->

    <div class="modal modal-danger fade" id="modalEliminar" name="modalEliminar" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-danger">
                <div class="modal-header" id="titl">
                    <h4 class="modal-title" id="titleModalEliminar">¡Eliminar Menú!</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h5 style="font-family:Verdana; font-size: 14px;" id="bodyModalEliminar" class="debug-url">
                        ¿Está seguro de que desea eliminar el plato?
                    </h5>
                    <form action="" class="form-modal" method="POST">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-outline-light btn-ok">Eliminar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    {{--<!-- DataTables -->--}}
    {{--<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>--}}
    {{--<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>--}}

    {{--<!-- page script -->--}}
    {{--<script>--}}
    {{--$(function () {--}}
    {{--$('#categories').DataTable({});--}}
    {{--});--}}
    {{--</script>--}}

    <!-- Modal Eliminar -->
    <script>
        $('#modalEliminar').on('show.bs.modal', function (e) {
            $(this).find('.form-modal').attr('action', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').click(function () {
                $("form").submit();
            });
        });
    </script>

@endsection