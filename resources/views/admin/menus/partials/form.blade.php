<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 20:22
 */
?>

<div class="card-body">

    <div class="row">
        <div class="form-group col-md-12">
            {!! Form::label('description', 'Descripción (opcional)') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Introduce la descripción del menú' ]) !!}

            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            {!! Form::label('price', 'Precio (opcional)') !!}

            <span style="color: red; float: right;"> Poner decimales con punto . </span>

            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => '4.50']) !!}

            @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group col-md-6">
            <label for="dateMenu">Fecha (opcional)</label>

            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control @error('dateMenu') is-invalid @enderror"
                       data-inputmask-alias="datetime"
                       data-inputmask-inputformat="dd/mm/yyyy" data-mask im-insert="false"
                       id="dateMenu" name="dateMenu"
                       placeholder="dd/mm/yyyy"
                       value="@php if($edit == 0) echo old('dateMenu');  else echo old('dateMenu', $dateMenu);  @endphp">

                {{--{!! Form::text('dateMenu', $dateMenu, null, ['class' => 'form-control', 'data-inputmask-alias' => 'datetime', 'data-inputmask-inputformat' => 'dd-mm-yyyy',--}}
                {{--'data-mask im-insert' => false, 'placeholder' => 'dd/mm/yyyy']) !!}--}}

                {{--{!! Form::text('dateMenu', null, ['class' => 'form-control', 'data-inputmask-alias' => 'datetime', 'data-inputmask-inputformat' => 'dd/mm/yyyy',--}}
                {{--'data-mask im-insert' => false]) !!}--}}

                @error('dateMenu')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
                @enderror
            </div>
        </div>
    </div>


    {{--<div class="form-group col-md-4">--}}
        {{--<label for="dateMenu">Fecha (opcional)</label>--}}

        {{--<div class="input-group">--}}
            {{--<div class="input-group-prepend">--}}
                {{--<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>--}}
            {{--</div>--}}
            {{--<input type="text" class="form-control @error('dateMenu') is-invalid @enderror"--}}
                   {{--data-inputmask-alias="datetime"--}}
                   {{--data-inputmask-inputformat="dd/mm/yyyy" data-mask im-insert="false"--}}
                   {{--id="dateMenu" name="dateMenu"--}}
                   {{--placeholder="dd/mm/yyyy"--}}
                   {{--value="@php if($edit == 0) echo old('dateMenu');  else echo old('dateMenu', $dateMenu);  @endphp">--}}

            {{--{!! Form::text('dateMenu', $dateMenu, null, ['class' => 'form-control', 'data-inputmask-alias' => 'datetime', 'data-inputmask-inputformat' => 'dd-mm-yyyy',--}}
                 {{--'data-mask im-insert' => false, 'placeholder' => 'dd/mm/yyyy']) !!}--}}

            {{--{!! Form::text('dateMenu', null, ['class' => 'form-control', 'data-inputmask-alias' => 'datetime', 'data-inputmask-inputformat' => 'dd/mm/yyyy',--}}
                 {{--'data-mask im-insert' => false]) !!}--}}

            {{--@error('dateMenu')--}}
            {{--<span class="invalid-feedback" role="alert">--}}
                {{--<strong>{{ $message }}</strong>--}}
            {{--</span>--}}
            {{--@enderror--}}
        {{--</div>--}}
        {{--<!-- /.input group -->--}}
    {{--</div>--}}

    <div class="row">
        <div class="form-group col-md-4">
            {!! Form::label('type_id', 'Tipo') !!}

            {!! Form::select('type_id', $types, null, ['class' => 'form-control', 'placeholder' => 'Selecciona un tipo de menú', 'required' => 'required']) !!}

            @error('types')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <div class="row">
    {{--<div class="checkbox col-md-6">--}}
    <div class="form-group col-md-6">
        <p class="font-weight-bold">Categorías</p>
        @foreach ($categories as $category)
            <label class="mr-2">
                {!! Form::checkbox('categories[]', $category->id, null) !!}
                {{$category->name}}

            </label>
        @endforeach

        @error('categories')
        <br>
        <small class="text-danger">{{$message}}</small>
        @enderror

    </div>
    </div>
</div>
<!-- /.card-body -->


@section('scripts')
    @parent

    <!-- InputMask -->
    <script src="{{ asset('adminZone/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('adminZone/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

    <script>

        //datemask dd/mm/yyyy
        // $('#dateMenu').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        $('#dateMenu').inputmask('dd/mm/yyyy')

    </script>

@endsection
