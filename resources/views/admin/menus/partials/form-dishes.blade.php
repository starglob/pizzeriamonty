<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 20:22
 */
?>

<div class="card-body">

    <div>
        <h3>{{ $menu->type->name }}</h3>
    </div>

    {!! Form::hidden('menu_id', $menu->id) !!}
    {!! Form::hidden('numCategories', $numCategories) !!}

    @for ($i=0; $i < $numCategories; $i++)

        {!! Form::hidden('categories_id[]', $selectedCategories[$i]->id) !!}
        {!! Form::hidden('categories_name[]', $selectedCategories[$i]->name) !!}

        <div class="form-group col-md-10">
            <p><h4 class="text-center">{{ $selectedCategories[$i]->name }}</h4></p>
        </div>

        <div class="form-group col-md-10">

            {!! Form::label('dishes', 'Platos') !!}

            @if ($editar == 0)
                {!! Form::select('dishes_'.$selectedCategories[$i]->name.'[]', $dishes, null, ['class' => 'form-control, select2bs4',
                'multiple' => 'multiple', 'data-placeholder' => 'Selecciona los platos', 'style' => 'width: 100%;', 'required' => 'required']) !!}

            @else

                <select class="form-control, select2bs4" name="{{ 'dishes_'.$selectedCategories[$i]->name.'[]' }}"
                        multiple data-placeholder="Selecciona los platos" style="width: 100%;" required>

                    @foreach($dishes as $dish)
                        <option value="{{ $dish->id }}"

                                {{-- Apareceran seleccionados los platos del menú en el select de la categoría correspondiente --}}
                                @foreach($dishes_menu as $dm)
                                    @foreach($dm as $d)
                                        @if (($d->dish_id == $dish->id) and ($d->category_id == $selectedCategories[$i]->id))
                                            selected
                                        @endif
                                    @endforeach
                                @endforeach

                        >{{ $dish->name }}</option>

                    @endforeach

                </select>

            @endif

        </div>

    @endfor


</div>
<!-- /.card-body -->


@section('scripts')
    @parent

    <!-- Select2 -->
    <script src="{{ asset('adminZone/plugins/select2/js/select2.full.min.js') }}"></script>

    <script>

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })

    </script>

@endsection
