<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 19:57
 */
?>
@extends('layouts.admin')

@section('breadcrumb')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $titlePage }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Página Principal</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection

@section('contenido')

    @if($errors->any())

        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>

    @endif

    <div class="card">

        {!! Form::model($dish,['route' => ['dishes.update', $dish], 'autocomplete' => 'off', 'method' => 'put']) !!}

        @include('admin.dishes.partials.form')

        <div class="card-footer">
            {{--<button type="submit" class="btn btn-primary">{{ __('Crear Alérgeno') }}</button>--}}
            {!! Form::submit('Editar Plato', ['class' => 'btn btn-primary'])!!}

            <a class="btn btn-link" href="{{ url()->previous() }}">{{ __('Volver') }}</a>
        </div>

        {!! Form::close() !!}
    </div>
@endsection

