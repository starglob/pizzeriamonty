<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 26/04/2021
 * Time: 20:22
 */
?>

<div class="card-body">
    <div class="form-group">
        {{--<label for="name">Nombre</label>--}}
        {!! Form::label('name', 'Nombre') !!}

        {{--<input type="text" class="form-control @error('name') is-invalid @enderror" id="name"--}}
               {{--name="name" placeholder="Nombre" value="{{ old('name') }}" required>--}}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Introduce el nombre del plato', 'required' => 'required']) !!}

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('ingredients', 'Ingredientes (opcional)') !!}

        {!! Form::textarea('ingredients', null, ['class' => 'form-control', 'placeholder' => 'Introduce los ingredientes del plato']) !!}

        @error('ingredients')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        {!! Form::label('price', 'Precio (opcional)') !!}

        {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => '4.50']) !!}
        {{--{!! Form::number('price', null, ['class' => 'form-control', 'placeholder' => '4.50']) !!}--}}

        @error('price')
        <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    {{--<div class="form-group">--}}
    <div class="checkbox col-sm-6">
        <p class="font-weight-bold">Alérgenos (opcional):</p>
        @php $cont = 1; @endphp
        @foreach ($allergens as $allergen)
            <label class="mr-2">
                {!! Form::checkbox('allergens[]', $allergen->id, null) !!}
                {{--<div class="row-3">--}}
                <span style="color: #ff0000;">{{ $cont }}</span> {{$allergen->name}}
                {{--</div>--}}
            </label>
            @php $cont++; @endphp
        @endforeach

        @error('allergens')
        <br>
        <small class="text-danger">{{$message}}</small>
        @enderror

    </div>

</div>
<!-- /.card-body -->
