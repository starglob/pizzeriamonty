<?php
/**
 * Created by PhpStorm.
 * User: Starglob2
 * Date: 21/04/2021
 * Time: 19:44
 */
?>
@extends('layouts.admin')

@section('contenido')
    @parent

    <!-- Info boxes -->
    <div class="row">

        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="nav-icon fas fa-allergies"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Alérgenos</span>
                    <span class="info-box-number">{{ $numAllergens }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="nav-icon fas fa-hamburger"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Platos</span>
                    <span class="info-box-number">{{ $numDishes }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-sm-6 col-md-3 col-lg-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-gray elevation-1"><i class="fas fa-utensils"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Menús</span>
                    <span class="info-box-number">{{ $numMenus }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Menú del día</h3>
                    <div class="card-tools">
                        {{--<button type="button" class="btn btn-tool" title="Editar">--}}
                            {{--<i class="fas fa-edit"></i></button>--}}

                        <a class="btn btn-tool" href="{{ route('menus.edit', $menuDia) }}" title="Editar"><i
                                    class="fas fa-edit"></i></a>
                        <a href="{{ route('admin.printMDia') }}" class="btn btn-tool" title="Imprimir Menú del día"><i class="fa fa-print"></i></a>

                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Minimizar">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                                title="Cerrar">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    @if (!isset($menuDia))
                        <p class="m-6 text-center text-danger"> No hay menús del día creados</p>

                    @else
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <h5 style="float: right;">{{ date('d/m/Y', strtotime($menuDia->dateMenu)) }}</h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <img src="{{ asset("assets/pizzeria-monty/images/menuDia.svg") }}" alt="Menú del día" width="350">
                            </div>
                        </div>

                        <!-- Obteniendo las categorías y platos asociadas al Menú del día -->

                        @for ($i=0; $i < $numCategories; $i++)

                            <div class="mt-5">
                                <p><h4 class="text-center" style="color: #FE7A13;">{{ $categories[$i]->name }}</h4></p>
                            </div>

                            @foreach($dishes as $dish)
                                @foreach($dishes_menu as $dm)
                                    @if (($dm->dish_id == $dish->id) and ($dm->category_id == $categories[$i]->id))
                                        <div>
                                            <p><h5 class="text-left ml-4">{{ $dish->name }} @foreach($dish->allergens as $dish->allergen) <img src="{{ Storage::url($dish->allergen->image->url) }}" alt="alergenos" width="30px;"> @endforeach</h5></p>
                                            <p><h6 class="text-center mb-5">@if($dish->ingredients) Contiene: {{ $dish->ingredients }}@endif</h6></p>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach

                        @endfor

                        <h6 class="mt-4">{{ $menuDia->description }}</h6>
                        <h3 class="text-center">{{ $menuDia->price }} €</h3>

                    @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Menú degustación</h3>
                            <div class="card-tools">
                                {{--<button type="button" class="btn btn-tool" title="Editar">--}}
                            {{--<i class="fas fa-edit"></i></button>--}}
                                <a class="btn btn-tool" href="{{ route('menus.edit', $menuDegustacion) }}" title="Editar"><i
                                    class="fas fa-edit"></i></a>
                                <a href="{{ route('admin.printMDegus') }}" class="btn btn-tool" title="Imprimir Degustación del menú"><i class="fa fa-print"></i></a>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Minimizar"><i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                                    title="Cerrar"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (!isset($menuDegustacion))
                                <p class="m-6 text-center text-danger"> No hay menús de degustación creados</p>
                            @else

                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <h5 style="float: right;">{{ date('d/m/Y', strtotime($menuDegustacion->dateMenu)) }}</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <img src="{{ asset("assets/pizzeria-monty/images/MenuDegustacion.svg") }}" alt="Menú degustación" width="350">
                                    </div>

                                </div>

                                <!-- Obteniendo las categorías asociadas al Menú degustación -->
                                @for ($j=0; $j < $numCategoriesMD; $j++)

                                    <div class="mt-5">
                                        <p><h4 class="text-center" style="color: #FE7A13;">{{ $categoriesMD[$j]->name }}</h4></p>
                                    </div>

                                    @foreach($dishes as $dish)
                                        @foreach($dishes_menuD as $dmDeg)
                                            @if (($dmDeg->dish_id == $dish->id) and ($dmDeg->category_id == $categoriesMD[$j]->id))
                                                <div>
                                                    <p><h5 class="text-center ">{{ $dish->name }} @foreach($dish->allergens as $dish->allergen) <img src="{{ Storage::url($dish->allergen->image->url) }}" alt="alergenos" width="30px;"> @endforeach</h5></p>
                                                    <p><h6 class="text-center mb-5">@if($dish->ingredients) Contiene: {{ $dish->ingredients }}@endif</h6></p>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach

                                @endfor


                                <h6 class="mt-4">{{ $menuDegustacion->description }}</h6>
                                <h3 class="text-center">{{ $menuDegustacion->price }} €</h3>

                            @endif

                        </div>
                    </div>
                </div>
            </div>

@endsection
