<?php
/**
 * Created by PhpStorm.
 * User: fabre
 * Date: 14/07/2021
 * Time: 21:16
 */
?>

    <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Pizzer&iacute;a Monty - Albarrac&iacute;n - Teruel">
    <meta name="author" content="Starglob Soluciones Inform&aacute;ticas">
    <link rel="icon" href="{{ asset('assets/pizzeria-monty/images/favicon/favico.ico') }}">
    <title>Pizzer&iacute;a Monty - Albarrac&iacute;n - Teruel - {{ $titlePage }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ asset('assets/additional/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/pizzeria-monty/css/bella-firenze.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{ asset('assets/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/owl-carousel/owl.theme.css') }}" rel="stylesheet">

    <!-- Animate CSS -->
    <link href="{{ asset('assets/animate-css/animate.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Engagement' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/bootstrap/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Menu Header -->
<section id="menu-header">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="mrgn-50-top">Pizzeria Monty - {{ $title }}</h2>
                <div class="mrgn-20-top-btm text-center"><img
                        src="{{ asset('assets/pizzeria-monty/images/dividers/divider-white.svg') }}" alt=""/></div>
            </div>
        </div>
        <!-- /.Title & Divider -->
    </div>
    <!-- /.container -->
</section>
<!-- /. Menu Header -->

<!-- Full Menu List -->
<section id="menu-list">
    <div class="container">

        <!-- Title & Divider -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeIn">
                <h2>Men&uacute;</h2>
                <p class="lead-sm">Nuestro men&uacute; siempre utiliza productos de temporada para que nuestros platos
                    sean lo más frescos y saludables posibles, así como productos locales otorgando un sabor
                    inigualable.</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img
                src="{{ asset('assets/pizzeria-monty/images/dividers/divider.svg') }}" alt=""/></div>
        <!-- /.Title & Divider -->

        @if($menus->isEmpty())
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="fas fa-exclamation-triangle"></i> ¡Atención!</h4>
                No existen datos que mostrar
            </div>
            {{--<div class="row">
                <div class="col-lg-12">
                    <a href="{{ route('menus.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo menú</a>
                </div>
            </div>--}}
            <br>

        @else

            <div class="row">
                <div class="col-md-12">

                    <!-- Menu Filter Area -->

                {{--<div class="row text-center">
                    <div class="col-lg-12">
                        <div class="menu-filter-area">
                            <ul class="menu-filter" id="menu-masonry-sort">
                                <li class="active"><a href="#" data-target="*">Todos</a></li>
                                <li><a href="#" data-target=".starters">Entrantes</a></li>
                                <li><a href="#" data-target=".salads">Ensaladas</a></li>
                                <li><a href="#" data-target=".pizzas">Pizzas</a></li>
                                <li><a href="#" data-target=".mains">Principales</a></li>
                                <li><a href="#" data-target=".desserts">Postres</a></li>
                            </ul>
                        </div>
                    </div>
                </div>--}}

                <!-- /.Menu Filter Area -->

                    <!-- Menu Items Area -->

                    <div class="row">
                        <div class="menu-items-wrapper col-lg-12">
                            <div class="menu-items" id="menu-masonry">
                                @foreach($menus as $menu)
                                    {{--<div class="col-xs-5 col-sm-3 col-md-4 col-lg-4">--}}

                                    <div class="single-item">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <h2>{{ $menu->type->name }}</h2>
                                        </div>
                                        <div class="row">
                                            {{--<div class="col-xs-7 col-sm-9 col-md-8 col-lg-8">--}}
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                {{--@foreach($menu->dishes as $dish)  <h3>{{ $dish->name }}</h3>--}}

                                                @php $catName= " "; @endphp
                                                @foreach($menu->categories as $category)
                                                    @if($category->name <> $catName)
                                                        <h3 style="text-align: center; margin-top: 20px; font-size:30px; font-weight:bold; ">{{ $category->name }}</h3>
                                                        <br>
                                                        @php $catName = $category->name; @endphp

                                                        @foreach($dishes_menus as $item)
                                                            @foreach($item as $dm)
                                                                @php $dishName= " ";  @endphp

                                                                @foreach($menu->dishes as $dish)
                                                                    {{--@if (($dm->dish_id == $dish->id) and ($dm->category_id == $category->id))--}}
                                                                    @if (($dm->dish_id == $dish->id) and ($dm->category_id == $category->id) and ($dish->name <> $dishName))
                                                                        {{--<ul class="menu-fd">--}}
                                                                            {{--<h3 style="text-align: center;">{{ $dish->name }}</h3>--}}
                                                                        {{--</ul>--}}
                                                                        <h3>{{ $dish->name }}</h3>

                                                                        @php $dishName = $dish->name; @endphp

                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                    <p>{{ $dish->ingredients }}</p>
                                                @endforeach


                                                {{--@endforeach--}}
                                                {{--<span class="accent">{{ $menu->price }} €</span>--}}
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>


                        </div>

                    </div><!-- /.Menu Items Area -->
                    @endif
                </div>
            </div>
            <!-- /.container -->
</section>
<!-- /.Full Menu List -->

<!-- Contact Information -->
<section id="contact">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeInDown">
                <h2>Contacta con nosotros</h2>
                <!--<p class="lead">Stob by whenever you would like a tasty bite and delicious glass of wine. Stay in touch on social media to know more about our special events.</p>-->
                <p class="lead">Puedes contactar con nosotros a trav&eacute;s de correo electr&oacute;nico o por tel&oacute;fono.
                    S&iacute;guenos en las redes sociales.</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img
                src="{{ asset('assets/pizzeria-monty/images/dividers/divider-white.svg') }}" alt=""/></div>
        <!-- Title & Divider -->

        <!-- Contact Information Content -->
        <div class="row">
            <!-- Address, Phone & Email -->
            <div class="col-md-5 col-md-offset-1 col-sm-5 wow fadeIn">
                <h3 class="mrgn-30-top ">Contacto</h3>
                <p class="mrgn-30-top lead">Pizzer&iacute;a Monty </p>
                <p class="lead">CALLE PORTAL DE MOLINA, 17, 44100 ALBARRACÍN, TERUEL </p>
                <p class="lead">978 71 01 45<br/>
                    <a href="mailto:pizzeriamonty@gmail.com">pizzeriamonty@gmail.com</a></p>
            </div>
            <!-- /.Address, Phone & Email -->

            <!-- Opening Times -->
            <div class="col-md-5 col-sm-7 wow fadeIn">
                <h3>Horario</h3>
                <div class="row">
                    <h4>Invierno</h4>
                    <div class="col-xs-5">
                        <ul class="list-unstyled weekdays">
                            <li>Viernes</li>
                            <li>S&aacute;bado</li>
                            <li>Domingo</li>
                        </ul>
                    </div>
                    <div class="col-xs-7">
                        <ul class="list-unstyled">
                            <li>20:00 - 22:30</li>
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                        </ul>
                    </div>
                    <h4>Verano</h4>
                    <div class="col-xs-5">
                        <ul class="list-unstyled weekdays">
                            <li>Todos los días</li>
                        </ul>
                    </div>
                    <div class="col-xs-7">
                        <ul class="list-unstyled">
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                        </ul>
                    </div>
                    <h4>Festivos y puentes consultar horario</h4>
                </div>
            </div>
            <!-- /.Opening Times -->
        </div>
        <!-- /.Contact Information Content -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Contact Information -->

<!-- Footer -->
<footer>
    <div class="container">
        <!-- Footer Content -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeIn">
                <!-- Restaurant Name & Divider -->
                <h2>Pizzer&iacute;a Monty</h2>
                <div class="mrgn-50-top-btm text-center"><img
                        src="{{ asset('assets/pizzeria-monty/images/dividers/divider-black.svg') }}" alt=""/></div>
                <!-- /.Restaurant Name & Divider -->

                <!-- Social Profiles -->
                <div class="mrgn-30-top-btm">
                    <ul class="list-inline social-buttons">
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a> </li>--}}
                        <li><a href="https://www.facebook.com/Pizzeria-Monty-1548467198730317/" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                        {{--<li><a href="#"><i class="fa fa-pinterest"></i></a> </li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a> </li>
                        <li><a href="#"><i class="fa fa-google"></i></a> </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a> </li>--}}
                    </ul>
                </div>
                <!-- /.Social Profiles -->
                <!-- Copyright -->
                <p>Pizzer&iacute;a Monty © <?php echo date('Y'); ?> | Todos los derechos reservados | Desarrollado por
                    <a href="http://www.starglob.com" target="_blank">Starglob Soluciones Inform&aacute;ticas</a></p>
                <!-- /.Copyright -->
            </div>
        </div>
        <!-- /.Footer Content -->
    </div>
    <!-- /.container -->
</footer>
<!-- /.Footer -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('assets/vendor/jquery.min.js') }}"><\/script>')</script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.j') }}s"></script>
<!-- Isotope JS -->
<script src="{{ asset('assets/isotope/isotope.pkgd.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- WOW JS -->
<script src="{{ asset('assets/wow/wow.min.js') }}"></script>
<!-- Bella Firenze-->
<script src="{{ asset('assets/pizzeria-monty/js/pizzeria-monty.js') }}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('assets/additional/ie10-viewport-bug-workaround.js') }}"></script>
</body>
</html>

