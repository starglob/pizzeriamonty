<?php
/**
 * Created by PhpStorm.
 * User: fabre
 * Date: 05/07/2021
 * Time: 13:13
 */
?>

    <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Pizzer&iacute;a Monty - Albarrac&iacute;n - Teruel">
    <meta name="author" content="Starglob Soluciones Inform&aacute;ticas">
    <link rel="icon" href="{{ asset('') }}assets/pizzeria-monty/images/favicon/favico.ico">
    <title>Pizzer&iacute;a Monty - Albarrac&iacute;n - Teruel</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ asset('assets/additional/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/pizzeria-monty/css/bella-firenze.css') }}" rel="stylesheet">

    <!-- Owl Carousel -->
    <link href="{{ asset('assets/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/owl-carousel/owl.theme.css') }}" rel="stylesheet">

    <!-- Animate CSS -->
    <link href="{{ asset('assets/animate-css/animate.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Engagement' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/bootstrap/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Main Navigation -->
<div id="mainnavigation">
    <!-- Nav -->
    <nav class="navbar navbar-fixed-top" data-spy="affix" data-offset-top="100">
        <div class="container">
            <div class="row hidden-sm hidden-xs">
                <!-- Links 01 -->
                <div class="col-sm-4">
                    <ul class="nav navbar-nav pull-left">
                        <li><a href="#about">Informaci&oacute;n</a></li>
                        <li><a href="#specials">Especialidades</a></li>
                        <!--<li><a href="#chefs">Chefs</a></li>-->
                        <li><a href="#menu">Menu</a></li>
                    </ul>
                </div>
                <!-- /.Links 01 -->
                <!-- Logo -->
                <div class="col-sm-4"> <a href="{{ route('home') }}" id="bellafirenze-lg"></a> </div>
                <!-- /.Logo -->

                <!-- Links 02 -->
                <div class="col-sm-4">
                    <ul class="nav navbar-nav pull-right">
                        <!--<li><a href="#events">Eventos</a></li>-->
                        <li><a href="#locations">Localizaci&oacute;n</a></li>
                        <!--<li><a href="#booktable">Reservas</a></li>-->
                        <li><a href="#contact">Contacto</a></li>
                    </ul>
                </div>
                <!-- Links 02 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->

        <!-- Nav for small screens -->
        <div class="container-fluid no-padding">
            <div class="visible-sm visible-xs">
                <!-- Navbar Header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar first"></span> <span class="icon-bar hide-bar"></span> <span class="icon-bar last"></span> </button>
                    <a class="navbar-brand" href="{{ route('home') }}" id="bellafirenze-sm"></a> </div>
                <!-- /.Navbar Header -->
                <!-- Navbar Collapse -->
                <div class="collapse navbar-collapse" id="navbar_collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#about">Informaci&oacute;n</a></li>
                        <li><a href="#specials">Especialidades</a></li>
                        <!--<li><a href="#chefs">Chefs</a></li>-->
                        <li><a href="#menu">Menu</a></li>
                        <!--<li><a href="#events">Eventos</a></li>-->
                        <li><a href="#locations">Localizaci&oacute;n</a></li>
                        <!--<li><a href="#booktable">Reservas</a></li>-->
                        <li><a href="#contact">Contacto</a></li>
                    </ul>
                </div>
                <!-- /.Navbar Collapse -->
            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- /.Nav for small screens -->
    </nav>
    <!-- /.Nav -->
</div>
<!-- /.Main Navigation -->

<!-- Fullwidth Header -->
<header id="fullwidth" class="bellafirenze">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="header-text text-center">
                    <!-- Heading Title Area -->
                    <p class="wow fadeInDown"> <img src="{{ asset('assets/pizzeria-monty/images/badge/bella-firenze-hp-badge.svg') }}" width="300" height="268" class="img-responsive center-block" alt="" /> </p>
                    <!--<p class="wow fadeInDown"> <img src="assets/bella-firenze/images/logo/pizzeria_Monty.svg" width="300" height="268" class="img-responsive center-block" alt="" /> </p>-->
                    <!-- /.Heading Title Area -->
                    <p class="details text-uppercase text-bold mrgn-30-top wow fadeIn"> Calle Portal de Molina, 17, 44100 Albarracín, Teruel <br/>
                        Tel&eacute;fono: 978 71 01 45</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
</header>
<!-- /.Fullwidth Header -->

<!-- About us -->
<section id="about">
    <div class="container-fluid">
        <!-- Title & Divider -->
        <div class="row wow fadeInDown">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2>Qui&eacute;nes somos</h2>
                <p class="lead">Pizzer&iacute;a Monty abri&oacute; sus puertas en Albarrac&iacute;n en el a&ntilde;o 2015</p>
                <p class="lead"> Es un lugar ideal para degustar pastas, pizzas artesanales, carnes ecológicas criadas en
                    granja propia y carnes de caza de los Montes Universales. Disponemos de una sala de juegos
                    interactivos para los más pequeños, controlada por cámaras.</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider.svg') }}" alt="" /></div>
        <!-- Title & Divider -->

        <!-- About us content -->
        <div class="row">
            <!-- Image -->
            <div class="col-lg-6 wow fadeInLeft"><img src="{{ asset('assets/pizzeria-monty/images/dishes/pizza.jpg') }}" width="800" height="310" class="img-responsive center-block" alt=""/></div>
            <!-- /.Image -->
            <!-- About us description -->
            <div class="col-lg-6 wow fadeInRight">
                <p class="lead-sm mrgn-30-top-btm">Pizzer&iacute;a Monty es un restaurante que comenzó con mucha ilusi&oacute;n y amor.</p>
                <p class="lead-sm mrgn-30-top-btm">Hoy en d&iacute;a damos servicio a la ciudad de Albarrac&iacute;n y su comarca. </p>
                <p class="lead-sm mrgn-30-top-btm">Es el lugar ideal para degustar pizzas y pastas. Con productos de calidad, frescos y de la zona en la que nos encontramos para dar un toque especial a nuestros platos. </p>
            </div>
            <!-- About us description -->
        </div>
        <!-- /.About us content -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.About us -->

<!-- Specials -->
<section id="specials" class="no-padding">
    <div class="container-fluid">
        <!-- Specials Content -->
        <div class="row">
            <div class="col-lg-6 no-padding text-center">
                <div class="bg-darkblue pddn-100-top-btm wow fadeInLeft">
                    <!-- Title & Divider -->
                    <h2>Men&uacute; de la casa - 16 €</h2>
                    <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider.svg') }}" alt="" /></div>
                    <!-- /.Title & Divider -->

                    <!-- Special 01 -->
                    <div class="mrgn-30-top-btm">
                        <p><span class="label label-dishitem text-uppercase">Primero</span></p>
                        <!--<h3>Desayuno mediterr&aacute;neo - 3.00 €</h3>-->
                        <p><em>Ensalada del tiempo<br>Macarrones a la boloñesa<br>Sopa de cocido<br>Plato del dia<br>Embutido de ciervo. </em></p>
                    </div>
                    <!-- /.Special 01 -->

                    <!-- Special 02 -->
                    <div class="mrgn-30-top-btm">
                        <p><span class="label label-dishitem text-uppercase">Segundo</span></p>
                        <!--<h3>Delicia de Albarrac&iacute;n - 12.00 €</h3>-->
                        <p><em>Chuletas de cordero<br>Redondo de ciervo<br>Ciervo en escabeche<br>Hamburguesa de ciervo o de cordero<br>Salmón
                                <br>Cordero al horno +3€<br>Cabrito al horno +3€<br>Chuletón de ciervo +3€<br>Chuletón de cordero +3€</em></p>
                    </div>
                    <!-- /.Special 02 -->

                    <!-- Special 03 -->
                    <div class="mrgn-30-top-btm">
                        <p><span class="label label-dishitem text-uppercase">Postre</span></p>
                        <!--<h3>Noche italiana - 9.00 €</h3>-->
                        <p><em>pan, una bebida y café</em></p>
                    </div>
                    <!-- /.Special 03 -->

                </div>
            </div>
        </div>
        <!-- /.Specials Content -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.Specials -->

<!-- Recommendations -->
<section id="recommendations">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row text-center mrgn-40-top">
            <div class="col-sm-12">
                <h3 class="wow fadeInDown">Recomendaciones del Chef</h3>
            </div>
        </div>
        <!-- Title & Divider -->

        <!-- Recommendation Content -->
        <div class="row text-center">
            <div class="col-xs-10 col-xs-offset-1">
                <!-- Owl Carousel Recommendations -->
                <div id="owl-recommendation" class="owl-carousel">

                    <!-- Recomendation 01 -->

                    <div class="item wow fadeIn">
                        <div class="bg-white mrgn-40-top-btm pddn-10">
                            <h4>Cheese Monty</h4>
                            <img src="{{ asset('assets/pizzeria-monty/images/dishes/bf-dishes-01.jpg') }}" width="1000" height="500" class="img-responsive center-block" alt=""/>
                            <p class="mrgn-10-top"><span class="accent-price">10.50€</span></p>
                            <p class="text-muted">tomate, mozzarella, queso de cabra, (*)cordero ecológico, mermelada de pimiento rojo y orégano.</p>
                        </div>
                    </div>

                    <!-- /.Recomendation 01 -->

                    <!-- Recomendation 02 -->
                    <div class="item wow fadeIn">
                        <div class="bg-white mrgn-40-top-btm pddn-10">
                            <h4>Regañao</h4>
                            <img src="{{ asset('assets/pizzeria-monty/images/dishes/bf-dishes-02.jpg') }}" width="1000" height="500" class="img-responsive center-block" alt=""/>
                            <p class="mrgn-10-top"><span class="accent-price">9.50€</span></p>
                            <p class="text-muted">tomate, mozzarella, jamón serrano, pimiento rojo y orégano</p>
                        </div>
                    </div>
                    <!-- /.Recomendation 02 -->

                    <!-- Recomendation 03 -->
                    <div class="item wow fadeIn">
                        <div class="bg-white mrgn-40-top-btm pddn-10">
                            <h4>Monty</h4>
                            <img src="{{ asset('assets/pizzeria-monty/images/dishes/bf-dishes-03.jpg') }}" width="1000" height="500" class="img-responsive center-block" alt=""/>
                            <p class="mrgn-10-top"><span class="accent-price">9€</span></p>
                            <p class="text-muted">tomate, mozzarella, mermelada de pimiento rojo, (*) cordero ecológico y orégano</p>
                        </div>
                    </div>
                    <!-- /.Recomendation 03 -->

                    <!-- Recomendation 04 -->
                    <!--                            <div class="item wow fadeIn">
                                                    <div class="bg-white mrgn-40-top-btm pddn-10">
                                                        <h4>Pizza Queso Albarrac&iacute;n</h4>
                                                        <img src="assets/pizzeria-monty/images/dishes/bf-dishes-04.jpg" width="1000" height="500" class="img-responsive center-block" alt=""/>
                                                        <p class="mrgn-10-top"><span class="accent-price">9.00€</span></p>
                                                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ante ipsum, pulvinar nec vehicula sagittis, aliquet a purus</p>
                                                    </div>
                                                </div>-->
                    <!-- /.Recomendation 04 -->

                    <!-- Recomendation 05 -->
                    <div class="item wow fadeIn">
                        <div class="bg-white mrgn-40-top-btm pddn-10">
                            <h4>Cordero ecológico al horno</h4>
                            <img src="{{ asset('assets/pizzeria-monty/images/dishes/bf-dishes-05.jpg') }}" width="1000" height="500" class="img-responsive center-block" alt=""/>
                            <p class="mrgn-10-top"><span class="accent-price">13€</span></p>
                            <p class="text-muted">Cordero ecológico y criado en granja propia al horno.</p>
                        </div>
                    </div>
                    <!-- /.Recomendation 06 -->
                    <!-- Recomendation 06 -->
                    <div class="item wow fadeIn">
                        <div class="bg-white mrgn-40-top-btm pddn-10">
                            <h4>Cabrito ecológico al horno</h4>
                            <img src="{{ asset('assets/pizzeria-monty/images/dishes/bf-dishes-05.jpg') }}" width="1000" height="500" class="img-responsive center-block" alt=""/>
                            <p class="mrgn-10-top"><span class="accent-price">13€</span></p>
                            <p class="text-muted">Cabrito ecológico y criado en granja propia al horno.</p>
                        </div>
                    </div>
                    <!-- /.Recomendation 06 -->

                </div>
                <!-- /.Owl Carousel Recommendations -->
            </div>
        </div>
        <!-- /.Recommendations Content -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Recommendations -->

<!-- Menu -->
<section id="menu">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeInDown">
                <h2>Menu</h2>
                <p class="lead">Pizzer&iacute;a Monty posee un equipo maravilloso que adora la cocina y que da lo mejor de s&iacute; en cada plato.</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider.svg') }}" alt="" /></div>
        <!-- /.Title & Diver -->

        <!-- Menu Lists -->
        <div class="row">

            <!-- Menu List 01 -->
            <div class="col-md-4 wow fadeIn">
                <p class="text-center mrgn-30-top"><span class="label label-menugroup text-uppercase">Pizza</span></p>
                <h3 class="mrgn-50-top">Monty <span class="accent">9€</span></h3>
                <p>tomate, mozzarella, mermelada de pimiento rojo, (*) cordero ecológico y orégano.</p>
                <h3>Regañao <span class="accent">9.50€</span></h3>
                <p>tomate, mozzarella, jamón serrano, pimiento rojo y orégano.</p>
                <h3>Aragonesa <span class="accent">10€</span></h3>
                <p>tomate, mozzarella, longaniza y lomo de cerdo en conserva y orégano.</p>
                <h3>Cheese Monty <span class="accent">10.50€</span></h3>
                <p>tomate, mozzarella, queso de cabra, (*) cordero ecológico, mermelada de pimiento rojo y orégano.</p>
            </div>
            <!-- /.Menu List 01 -->

            <!-- Menu List 02 -->
            <div class="col-md-4 wow fadeIn">
                <p class="text-center mrgn-30-top"><span class="label label-menugroup text-uppercase">Pasta</span></p>
                <h3 class="mrgn-50-top">Boloñesa <span class="accent">7€</span></h3>
                <p>orégano, pimienta, carne picada, cebolla, zanahoria y tomate.</p>
                <h3>Carbonara <span class="accent">7.8€</span></h3>
                <p>orégano, pimienta, bacon, cebolla, champiñón y nata.</p>
                <h3>Marinera <span class="accent">8.50€</span></h3>
                <p>orégano, pimienta, almejas, mejillones, gambas, vino blanco, cebolla, caldo de pescado y tomate.</p>
                <h3>Con ajo <span class="accent">6.80€</span></h3>
                <p>orégano, pimienta, ajo y aceite.</p>
            </div>
            <!-- /.Menu List 02 -->

            <!-- Menu List 03 -->
            <div class="col-md-4 wow fadeIn">
                <p class="text-center mrgn-30-top"><span class="label label-menugroup text-uppercase">Postres</span></p>
                <h3 class="mrgn-50-top">Brownie <span class="accent">5€</span></h3>
                <p>con helado de vainilla.</p>
                <h3>Flan <span class="accent">3.5€</span></h3>
                <p>de queso.</p>
                <h3>Copa de helado de turrón <span class="accent">4€</span></h3>
                <p>nata, caramelo y frutas secas</p>
                <h3>Mouse <span class="accent">3.5€</span></h3>
                <p>de chocolate o limón</p>
            </div>
            <!-- /.Menu List 03 -->
        </div>
        <!-- /.Menu Lists -->

        <!-- Menu Button -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center mrgn-50-top"> <a href="{{ route('menu') }}" class="btn btn-lg btn-main" target="_blank">Ver men&uacute; completo</a> </div>
        </div>
        <!-- /.Menu Button -->
        <!-- Menu Button -->
        {{--<div class="row">
            <div class="col-md-8 col-md-offset-2 text-center mrgn-50-top"> <a href="menuCeliacos.html" class="btn btn-lg btn-main" target="_blank">Ver men&uacute; celiacos</a> </div>
        </div>--}}
        <!-- /.Menu Button -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Menu -->



<!-- Fresh Food CTA -->
<section id="fresh-food">
    <div class="container">
        <!-- Fresh Food Content -->
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center wow fadeInDown">
                <h2>Los productos deben ser frescos, deliciosos y bien presentados.</h2>
                <p class="lead mrgn-40-top-btm text-uppercase text-bold">Ven a Pizzer&iacute;a Monty.</p>
            </div>
        </div>
        <!-- /.Fresh Food Content -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Fresh Food CTA -->

<!-- Locations -->
<section id="locations">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeInDown">
                <h2>Localizaci&oacute;n</h2>
                <p class="lead">Calle Portal de Molina, 17, 44100 Albarracín, Teruel</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider.svg') }}" alt="" /></div>
        <!-- /.Title & Divider -->

        <!-- Locations List -->
        <div class="row text-center">

            <!-- Location 02 -->
            <div class="col-md-offset-4 col-md-4 wow fadeIn mrgn-40-btm"> <img src="{{ asset('assets/pizzeria-monty/images/locations/location02.jpg') }}" class="img-responsive location" alt="" />
                <h3>Pizzer&iacute;a Monty</h3>
                <p class="lead-sm mrgn-30-top"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3038.02388005609!2d-1.4466957843221242!3d40.40832176407414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd5dc69d94dc0011%3A0x4f70676c601b4860!2sCalle+Portal+de+Molina%2C+17%2C+44126+Albarrac%C3%ADn%2C+Teruel!5e0!3m2!1ses!2ses!4v1470648583053" width="360" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
                <!--<p class="mrgn-20-top"><a href="#booktable" class="btn btn-sm btn-main">Reserva una mesa</a></p>-->
            </div>
            <!-- /.Location 02 -->

        </div>
        <!-- /.Location List -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Locations -->

<!-- Contact Information -->
<section id="contact">
    <div class="container">
        <!-- Title & Divider -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeInDown">
                <h2>Contacta con nosotros</h2>
                <!--<p class="lead">Stob by whenever you would like a tasty bite and delicious glass of wine. Stay in touch on social media to know more about our special events.</p>-->
                <p class="lead">Puedes contactar con nosotros a trav&eacute;s de correo electr&oacute;nico o por tel&oacute;fono. S&iacute;guenos en las redes sociales.</p>
            </div>
        </div>
        <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider-white.svg') }}"  alt=""/></div>
        <!-- Title & Divider -->

        <!-- Contact Information Content -->
        <div class="row">
            <!-- Address, Phone & Email -->
            <div class="col-md-5 col-md-offset-1 col-sm-5 wow fadeIn">
                <h3 class="mrgn-30-top ">Contacto</h3>
                <p class="mrgn-30-top lead">Pizzer&iacute;a Monty </p>
                <p class="lead">CALLE PORTAL DE MOLINA, 17, 44100 ALBARRACÍN, TERUEL </p>
                <p class="lead">978 71 01 45<br/>
                    <a href="mailto:pizzeriamonty@gmail.com">pizzeriamonty@gmail.com</a></p>
            </div>
            <!-- /.Address, Phone & Email -->

            <!-- Opening Times -->
            <div class="col-md-5 col-sm-7 wow fadeIn">
                <h3>Horario</h3>
                <div class="row">
                    <h4>Invierno</h4>
                    <div class="col-xs-5">
                        <ul class="list-unstyled weekdays">
                            <li>Viernes</li>
                            <li>S&aacute;bado</li>
                            <li>Domingo</li>
                        </ul>
                    </div>
                    <div class="col-xs-7">
                        <ul class="list-unstyled">
                            <li>20:00 - 22:30</li>
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                        </ul>
                    </div>
                    <h4>Verano</h4>
                    <div class="col-xs-5">
                        <ul class="list-unstyled weekdays">
                            <li>Todos los días</li>
                        </ul>
                    </div>
                    <div class="col-xs-7">
                        <ul class="list-unstyled">
                            <li>13:00 - 15:30 / 20:00 - 22:30</li>
                        </ul>
                    </div>
                    <h4>Festivos y puentes consultar horario</h4>
                </div>
            </div>
            <!-- /.Opening Times -->
        </div>
        <!-- /.Contact Information Content -->
    </div>
    <!-- /.container -->
</section>
<!-- /.Contact Information -->

<!-- Footer -->
<footer>
    <div class="container">
        <!-- Footer Content -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center wow fadeIn">
                <!-- Restaurant Name & Divider -->
                <h2>Pizzer&iacute;a Monty</h2>
                <div class="mrgn-50-top-btm text-center"><img src="{{ asset('assets/pizzeria-monty/images/dividers/divider-black.svg') }}" alt="" /></div>
                <!-- /.Restaurant Name & Divider -->

                <!-- Social Profiles -->
                <div class="mrgn-30-top-btm">
                    <ul class="list-inline social-buttons">
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a> </li>--}}
                        <li><a href="https://www.facebook.com/Pizzeria-Monty-1548467198730317/" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                        {{--<li><a href="#"><i class="fa fa-pinterest"></i></a> </li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a> </li>
                        <li><a href="#"><i class="fa fa-google"></i></a> </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a> </li>--}}
                    </ul>
                </div>
                <!-- /.Social Profiles -->
                <!-- Copyright -->
                <p> <a href="{{ route('login_empresa') }}"><i class="fa fa-lock"></i></a>  Pizzer&iacute;a Monty © <?php echo date('Y'); ?> | Todos los derechos reservados | Desarrollado por <a href="http://www.starglob.com" target="_blank">Starglob Soluciones Inform&aacute;ticas</a></p>
                <!-- /.Copyright -->
            </div>
        </div>
        <!-- /.Footer Content -->
    </div>
    <!-- /.container -->
</footer>
<!-- /.Footer -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('assets/vendor/jquery.min.js') }}"><\/script>')</script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- WOW JS -->
<script src="{{ asset('assets/wow/wow.min.j') }}s"></script>
<!-- Pizzeria Monty-->
<script src="{{ asset('assets/pizzeria-monty/js/pizzeria-monty.js') }}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('assets/additional/ie10-viewport-bug-workaround.js') }}"></script>
</body>
</html>
