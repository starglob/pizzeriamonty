<?php

use App\Http\Controllers\Admin\AllergenController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DishController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\TypeController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PARTE PÚBLICA

/*
 * HOME
 */

//Route::get('/', function () {
//    $title = "Inicio";
//    return view('home', compact('title'));
//})->name('home');

Route::get('/', [PublicController::class, 'index'])->name('home');

Route::get('/menu', [PublicController::class, 'menu'])->name('menu');


/*
 * Acceso a Administración
 */
Route::get('login_empresa', function () {
    return view('auth.login');
})->name('login_empresa');


//Auth::routes();

// PARTE ADMINISTRACIÓN

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');


Route::group(['middleware' => ['auth']], function () {

    Route::get('admin/printMDia', [AdminController::class, 'printMDia'])->name('admin.printMDia');
    Route::get('admin/printMDegus', [AdminController::class, 'printMDegus'])->name('admin.printMDegus');

    Route::get('/admin', [AdminController::class, 'index'])->name('admin');

    Route::resource('/admin/allergens', AllergenController::class);

    Route::resource('/admin/categories', CategoryController::class);

    Route::resource('/admin/types', TypeController::class);

    Route::resource('/admin/dishes', DishController::class);

    //Route::get('menus/selectDishes', [MenuController::class, 'selectDishes'])->name('menu.selectDishes');


    Route::post('menus/selectDishes', [MenuController::class, 'storeDish'])->name('menus.storeDish');
    Route::put('menus/updateDishes', [MenuController::class, 'updateDish'])->name('menus.updateDish');

    Route::resource('/admin/menus', MenuController::class);


});
