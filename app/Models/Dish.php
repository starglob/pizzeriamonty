<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    use HasFactory;

    //protected $fillable = ['name', 'ingredients', 'price'];

    //aquí guardamos los campos que queremos evitar que se llenen por asignación masiva
    protected $guarded = ['id', 'created_at', 'updated_at'];

    //Relación muchos a muchos

    public function allergens()
    {
        return $this->belongsToMany(Allergen::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_dish_menu')
            //->withPivot('menu_id', 'status');
            ->withPivot('menu_id');
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'category_dish_menu')
            //->withPivot('category_id', 'status');
            ->withPivot('category_id');
    }


}
