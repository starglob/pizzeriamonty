<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allergen extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description'];

    //aquí guardamos los campos que queremos evitar que se llenen por asignación masiva
    //protected $guarded = ['id', 'created_at', 'updated_at'];

    //Relación muchos a muchos

    public function dishes(){
        return $this->belongsToMany(Dish::class);
    }

    //Relación uno a uno polimórfica

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

}
