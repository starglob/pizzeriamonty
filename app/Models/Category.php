<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description'];

    //Relación muchos a muchos

    public function dishes(){
        return $this->belongsToMany(Dish::class,'category_dish_menu')
            //->withPivot('menu_id', 'status');
            ->withPivot('menu_id');
    }

    public function menus(){
        return $this->belongsToMany(Menu::class, 'category_dish_menu')
            //->withPivot('dish_id', 'status');
            ->withPivot('dish_id');
    }
}
