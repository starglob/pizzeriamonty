<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    //protected $fillable = ['name'];

    //aquí guardamos los campos que queremos evitar que se llenen por asignación masiva
    protected $guarded = ['id', 'created_at', 'updated_at'];

    //Relación uno a muchos

    public function menu(){
        return $this->hasMany(Menu::class);
    }

}
