<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    //protected $fillable = ['name', 'price', 'description'];

    //aquí guardamos los campos que queremos evitar que se llenen por asignación masiva
    protected $guarded = ['id', 'created_at', 'updated_at'];

    //Relación muchos a muchos

    public function categories(){
        return $this->belongsToMany(Category::class,'category_dish_menu')
            //->withPivot('dish_id', 'status');
            ->withPivot('dish_id');
    }

    public function dishes(){
        return $this->belongsToMany(Dish::class,'category_dish_menu')
            //->withPivot('category_id', 'status');
            ->withPivot('category_id');
    }


    //Relación uno a muchos inversa
    public function type(){
        return $this->belongsTo(Type::class);
    }


    public function platos()
    {
        return $this->hasManyThrough(Dish::class, Category::class);
    }

}
