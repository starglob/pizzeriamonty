<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    public function index(){

        $title = "Inicio";

        //Obtenemos todos los platos
        $dishes = Dish::all( 'id', 'name', 'ingredients', 'price');

        /************************* Almuerzo ***************************/

        //$almuerzo = Menu::latest('id')->where('type_id', 5)
        //    ->first();

        //dd($almuerzo);

        /* Platos y categorías asociados al almuerzo */
        //$dishes_almuerzo = DB::table('category_dish_menu')
        //    ->select('dish_id', 'category_id')
        //    ->where('menu_id','=', $almuerzo->id)
        //    ->get();

        /* Categorías asociadas al almuerzo (sin duplicados) */
        //$categoriesAlmuerzo = DB::table('category_dish_menu')
        //    ->select ('category_id')
        //    ->where('menu_id','=', $almuerzo->id)
        //    ->distinct()
        //    ->get();

        //$numCategoriesAl = count($categoriesAlmuerzo);

        //Aquí guardaremos las categorías del menu
        //$categoriesAl = [];

        //for ($i = 0; $i < $numCategoriesAl; $i++) {

        //    $data = DB::table('categories')
        //        ->select('id', 'name')
        //        ->where('id', '=', $categoriesAlmuerzo[$i]->category_id)
        //        ->first();

        //    array_push($categoriesAl, $data);

        //}

        /************************ Menú del día ************************/

        $menuDia = Menu::latest('id')->where('type_id', 4)
            ->first();

        /* Platos y categorías asociados al menú del Día */
        $dishes_menu = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDia->id)
            ->get();

        /* Categorías asociadas al menú del Día (sin duplicados) */
        $categoriesMenu = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDia->id)
            ->distinct()
            ->get();

        $numCategories = count($categoriesMenu);

        //Aquí guardaremos las categorías del menu
        $categories = [];

        for ($i = 0; $i < $numCategories; $i++) {

            $data = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $categoriesMenu[$i]->category_id)
                ->first();

            array_push($categories, $data);

        }



        /************************ Menú Degustación ***************************/

        $menuDegustacion = Menu::latest('id')->where('type_id', 3)
            ->first();

        /* Platos y categorías asociados al menú Degustación */
        $dishes_menuD = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->get();

        //dd($dishes_menuD);

        /* Categorías asociadas al menú Degustación (sin duplicados) */
        $catMenuDegust = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->distinct()
            ->get();
        //dd($catMenuDegust);

        $numCategoriesMD = count($catMenuDegust);
        //dd($numCategoriesMD);

        //Aquí guardaremos las categorías del menu Degustación
        $categoriesMD = [];

        for ($i = 0; $i < $numCategoriesMD; $i++) {

            $dataMD = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $catMenuDegust[$i]->category_id)
                ->first();
            //dd($data);
            array_push($categoriesMD, $dataMD);

        }


        /************************* Carta ***************************/

        $carta = Menu::latest('id')->where('type_id', 2)
            ->first();

        /* Platos y categorías asociados a la carta */
        $dishes_carta = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $carta->id)
            ->get();

        /* Categorías asociadas a la carta (sin duplicados) */
        $categoriesC = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $carta->id)
            ->distinct()
            ->get();

        $numCategoriesCarta = count($categoriesC);

        //Aquí guardaremos las categorías del menu
        $categoriesCarta = [];

        for ($i = 0; $i < $numCategoriesCarta; $i++) {

            $data = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $categoriesC[$i]->category_id)
                ->first();

            array_push($categoriesCarta, $data);

        }


        /************************* Raciones ***************************/

        //$raciones = Menu::latest('id')->where('type_id', 1)
        //    ->first();

        /* Platos y categorías asociados a las raciones */
        //$dishes_raciones = DB::table('category_dish_menu')
        //    ->select('dish_id', 'category_id')
        //    ->where('menu_id','=', $raciones->id)
        //    ->get();

        //$groups_raciones = $dishes_raciones->split(2);

        /* Categorías asociadas a las raciones (sin duplicados) */
        //$categoriesRaciones = DB::table('category_dish_menu')
        //    ->select ('category_id')
        //    ->where('menu_id','=', $raciones->id)
        //    ->distinct()
        //    ->get();

        //$categoryR = DB::table('categories')
        //    ->select('id', 'name')
        //    ->where('id', '=', $categoriesRaciones[0]->category_id)
        //    ->get();


        /*return view('public.index', compact('title','almuerzo', 'categoriesAl', 'numCategoriesAl', 'dishes_almuerzo',
            'dishes', 'menuDia', 'categories', 'numCategories', 'dishes_menu', 'menuDegustacion', 'numCategoriesMD',
            'dishes_menuD', 'categoriesMD', 'raciones', 'categoryR', 'groups_raciones', 'carta', 'categoriesCarta',
            'numCategoriesCarta', 'dishes_carta' ));*/

        return view('public.index', compact('title', 'dishes', 'menuDia', 'categories', 'numCategories', 'dishes_menu', 'menuDegustacion', 'numCategoriesMD', 'dishes_menuD', 'categoriesMD', 'carta', 'categoriesCarta',
            'numCategoriesCarta', 'dishes_carta' ));


    }

    public function menu(){

        $titlePage = "Menús";
        $title = "Menús";

        $menus = Menu::all();

        $dishes_menus = [];

        foreach ($menus as $menu){
            /* Platos y categorías asociados a los menus */
            $dishes_menu = DB::table('category_dish_menu')
                ->select('dish_id', 'category_id')
                ->where('menu_id','=', $menu->id)
                ->get();

            array_push($dishes_menus, $dishes_menu);
        }

        //return view('public.menu', compact('menus', 'titlePage', 'title'));
        return view('public.menu', compact('menus', 'titlePage', 'title', 'dishes_menus'));
    }
}
