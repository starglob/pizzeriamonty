<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Allergen;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AllergenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "Gestión de Alérgenos";
        $title = "Alérgenos";

        $allergens = Allergen::where([
            [function ($query) use ($request){
                if(($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%'. $term . '%')->get();
                }
            }]
        ])
            ->paginate(5);

        return view('admin.allergens.index', compact('allergens', 'titlePage', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Creación de Alérgenos";
        $title = "Crear alérgeno";

        return view('admin.allergens.create', compact('titlePage', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $allergen = new Allergen();
        $img = new Image();

        //Allergen::create($request->all());
        $allergen->name = $request->name;
        $allergen->description = $request->description;

        $allergen->save();

        if ($request->file('file')){
            $url = Storage::put('allergens', $request->file('file'));
            $alternativeText = $request->alternativeText;

            $allergen->image()->create([
                'url' => $url,
                'alternativeText' => $alternativeText
            ]);
        }

        return redirect()->route('allergens.index')
            ->with('info', 'Alérgeno creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Allergen  $allergen
     * @return \Illuminate\Http\Response
     */
    public function show(Allergen $allergen)
    {
        $titlePage = "Mostrar detalle de Alérgeno";
        return view('admin.allergens.show', compact('allergen', 'titlePage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Allergen  $allergen
     * @return \Illuminate\Http\Response
     */
    public function edit(Allergen  $allergen)
    {
        $titlePage = "Editar Alérgeno";
        $title = "Alérgenos";

        return view('admin.allergens.edit', compact('allergen', 'titlePage', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Allergen $allergen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Allergen $allergen)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        //Allergen::create($request->all());
        $allergen->name = $request->name;
        $allergen->description = $request->description;

        $allergen->save();

        if ($request->file('file')){
            $url = Storage::put('allergens', $request->file('file'));
            $alternativeText = $request->alternativeText;

            if($allergen->image){ //si tiene asociada una imagen
                Storage::delete($allergen->image->url);

                $allergen->image()->update([
                    'url' => $url,
                    'alternativeText' => $alternativeText
                ]);

            }else{
                $allergen->image()->create([
                    'url' => $url,
                    'alternativeText' => $alternativeText
                ]);
            }
        }

        return redirect()->route('allergens.index')
            ->with('info', 'Alérgeno actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Allergen $allergen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Allergen $allergen)
    {
        try {
            $allergen->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return redirect()->route('allergens.index')
            ->with('info', 'Alérgeno eliminado con éxito.');
    }
}
