<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Allergen;
use App\Models\Dish;
use Illuminate\Http\Request;

class DishController extends Controller
{
    protected $paginationTheme = 'bootstrap';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "Gestión de Platos";
        $title = "Platos";

        $dishes = Dish::where([
            [function ($query) use ($request){
                if(($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%'. $term . '%')->get();
                }
            }]
        ])
            ->paginate(5);

        return view('admin.dishes.index', compact('dishes', 'titlePage', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Creación de Platos";
        $title = "Crear plato";
        $allergens = Allergen::all();

        return view('admin.dishes.create', compact('titlePage', 'title', 'allergens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'name' => 'required',
            //'ingredients' => 'required'
        ]);

        //$dish = new Dish();
        //$dish->price = number_format($request->price, 2, ",", ".");
        $dish = Dish::create($request->all());

        //si estamos mandando información de alérgenos
        if($request->allergens){
            $dish->allergens()->attach($request->allergens);
        }

        return redirect()->route('dishes.index')
            ->with('info', 'Plato creado con éxito.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function edit(Dish $dish)
    {
        $titlePage = "Edición de Platos";
        $title = "Plato";
        $allergens = Allergen::all();

        return view('admin.dishes.edit', compact('dish', 'allergens', 'titlePage', 'title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dish $dish)
    {
        $request->validate([
            'name' => 'required',
            //'ingredients' => 'required'
        ]);

        $dish->update($request->all());

        //dd($request->allergens);
        //si estamos mandando información de alérgenos
        if($request->allergens) {
            //$dish->allergens()->attach($request->allergens);
            $dish->allergens()->sync($request->allergens); //ponemos sync para que guarde marcados y desmarcados
        } else{
            $dish->allergens()->attach($request->allergens);
        }

        return redirect()->route('dishes.index')
            ->with('info', 'Plato editado con éxito.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish)
    {
        try {
            $dish->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return redirect()->route('dishes.index')
            ->with('info', 'Plato eliminado con éxito.');
    }
}
