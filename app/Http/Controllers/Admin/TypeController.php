<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    protected $paginationTheme = 'bootstrap';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "Gestión de Tipos de menú";
        $title = "Tipos de menú";

        $types = Type::where([
            [function ($query) use ($request){
                if(($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%'. $term . '%')->get();
                }
            }]
        ])
            ->paginate(5);

        return view('admin.types.index', compact('types', 'titlePage', 'title'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Creación de Tipo";
        $title = "Crear tipo de menú";

        return view('admin.types.create', compact('titlePage', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Type::create($request->all());

        return redirect()->route('types.index')
            ->with('info', 'Tipo creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        $titlePage = "Mostrar detalle de Tipo";
        $title = "Tipo de menú";

        return view('admin.types.show', compact('type', 'titlePage', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        $titlePage = "Editar Tipo";
        $title = "Tipo de menú";

        return view('admin.types.edit', compact('type', 'titlePage', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $request->validate([
            'name' => 'required',
            //'description' => 'required'
        ]);

        $type->update($request->all());

        return redirect()->route('types.index')
            ->with('info', 'Tipo actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        try {
            $type->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return redirect()->route('types.index')
            ->with('info', 'Tipo eliminado con éxito.');
    }
}
