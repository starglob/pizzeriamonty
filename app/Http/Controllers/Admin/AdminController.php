<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Allergen;
use App\Models\Dish;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index(Request $request){

        $numAllergens = Allergen::count();
        $numDishes = Dish::count();
        $numMenus = Menu::count();

        $menuDia = Menu::latest('id')->where('type_id', 4)
            ->first();

        /* Platos y categorías asociados al menú del Día */
        $dishes_menu = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDia->id)
            ->get();

        /* Categorías asociadas al menú del Día (sin duplicados) */
        $categoriesMenu = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDia->id)
            ->distinct()
            ->get();

        $numCategories = count($categoriesMenu);

        //Aquí guardaremos las categorías del menu
        $categories = [];

        for ($i = 0; $i < $numCategories; $i++) {

            $data = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $categoriesMenu[$i]->category_id)
                ->first();

            array_push($categories, $data);

        }

        //Obtenemos todos los platos
        $dishes = Dish::all( 'id', 'name', 'ingredients');


        /********************** MENU DEGUSTACION **********************/

        $menuDegustacion = Menu::latest('id')->where('type_id', 3)
            ->first();

        /* Platos y categorías asociados al menú Degustación */
        $dishes_menuD = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->get();

        /* Categorías asociadas al menú Degustación (sin duplicados) */
        $catMenuDegust = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->distinct()
            ->get();

        $numCategoriesMD = count($catMenuDegust);

        //Aquí guardaremos las categorías del menu Degustación
        $categoriesMD = [];

        for ($i = 0; $i < $numCategoriesMD; $i++) {

            $dataMD = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $catMenuDegust[$i]->category_id)
                ->first();
            array_push($categoriesMD, $dataMD);

        }

        return view('admin.index', compact('numAllergens','numDishes', 'numMenus', 'menuDia',
            'menuDegustacion', 'categories', 'numCategories', 'dishes_menu', 'dishes', 'numCategoriesMD',
            'dishes_menuD', 'categoriesMD'));


    }


    public function printMDia()
    {
        $menuDia = Menu::latest('id')->where('type_id', 4)
            ->first();

        /* Platos y categorías asociados al menú del Día */
        $dishes_menu = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDia->id)
            ->get();

        /* Categorías asociadas al menú del Día (sin duplicados) */
        $categoriesMenu = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDia->id)
            ->distinct()
            ->get();

        $numCategories = count($categoriesMenu);

        //Aquí guardaremos las categorías del menu
        $categories = [];

        for ($i = 0; $i < $numCategories; $i++) {

            $data = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $categoriesMenu[$i]->category_id)
                ->first();

            array_push($categories, $data);

        }

        //Obtenemos todos los platos
        $dishes = Dish::all( 'id', 'name', 'ingredients');

        return view('admin.printMDia', compact('menuDia', 'categories', 'numCategories', 'dishes_menu', 'dishes'));

    }

    public function printMDegus()
    {
        $menuDegustacion = Menu::latest('id')->where('type_id', 3)
            ->first();

        //Obtenemos todos los platos
        $dishes = Dish::all( 'id', 'name', 'ingredients');

        /* Platos y categorías asociados al menú Degustación */
        $dishes_menuD = DB::table('category_dish_menu')
            ->select('dish_id', 'category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->get();

        /* Categorías asociadas al menú Degustación (sin duplicados) */
        $catMenuDegust = DB::table('category_dish_menu')
            ->select ('category_id')
            ->where('menu_id','=', $menuDegustacion->id)
            ->distinct()
            ->get();

        $numCategoriesMD = count($catMenuDegust);

        //Aquí guardaremos las categorías del menu Degustación
        $categoriesMD = [];

        for ($i = 0; $i < $numCategoriesMD; $i++) {

            $dataMD = DB::table('categories')
                ->select('id', 'name')
                ->where('id', '=', $catMenuDegust[$i]->category_id)
                ->first();

            array_push($categoriesMD, $dataMD);

        }

        return view('admin.printMDegus', compact('menuDegustacion','dishes', 'numCategoriesMD',
                'dishes_menuD', 'categoriesMD'));
    }

}
