<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $paginationTheme = 'bootstrap';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "Gestión de Categorías";
        $title = "Categorías";

        $categories = Category::where([
            [function ($query) use ($request){
                if(($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%'. $term . '%')->get();
                }
            }]
        ])
            ->paginate(6);

        return view('admin.categories.index', compact('categories', 'titlePage', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Creación de Categoría";
        $title = "Crear categoría";

        return view('admin.categories.create', compact('titlePage', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            //'description' => 'required'
        ]);

        Category::create($request->all());

        return redirect()->route('categories.index')
            ->with('info', 'Categoría creada con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $titlePage = "Mostrar detalle de Categoría";

        return view('admin.categories.show', compact('category', 'titlePage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $titlePage = "Editar Categoría";
        $title = "Categorías";

        return view('admin.categories.edit', compact('category', 'titlePage', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            //'description' => 'required'
        ]);

        $category->update($request->all());

        return redirect()->route('categories.index')
            ->with('info', 'Categoría actualizada con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return redirect()->route('categories.index')
            ->with('info', 'Categoría eliminada con éxito.');
    }

}
