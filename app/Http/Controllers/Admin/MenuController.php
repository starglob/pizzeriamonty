<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Dish;
use App\Models\Menu;
use App\Models\Type;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;

class MenuController extends Controller
{
    protected $paginationTheme = 'bootstrap';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "Gestión de Menús";
        $title = "Menús";

        $menus = Menu::where([
            [
                function ($query) use ($request) {
                    if (($term = $request->term)) {
                        $query->orWhere('name', 'LIKE', '%' . $term . '%')->get();
                    }
                }
            ]
        ])
            ->paginate(4);

        return view('admin.menus.index', compact('menus', 'titlePage', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Creación de Menús";
        $title = "Crear menú";
        //$categories = Category::pluck('name', 'id');
        $categories = Category::all();
        $types = Type::pluck('name', 'id');
        //$dishes = Dish::pluck('name', 'id');

        //return view('admin.menus.create', compact('titlePage', 'title', 'categories', 'types', 'dishes'));
        return view('admin.menus.create', compact('titlePage', 'title', 'categories', 'types'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'type_id' => 'required',
            'categories' => 'required'
        ]);

        $menu = new Menu();

        $menu->description = $request->description;
        if($request->price){
            $menu->price = str_replace(',', '.', $request->price);
        }
        if($request->dateMenu){
            $menu->dateMenu = Carbon::createFromFormat('d/m/Y', $request->dateMenu);
        }
        $menu->type_id = $request->type_id;
        $menu->save();

        $categories = $request->categories;
        $numCategories = count($categories);

        $selectedCategories = [];
        $data = [];

        for ($i = 0; $i < $numCategories; $i++) {
            //dd($categories[$i]);
            $data = DB::table('categories')
                ->select('id', 'name')
                //->select('name')
                ->where('id', $categories[$i])
                //->get();
                ->first();

            array_push($selectedCategories, $data);
        }

        $titlePage = "Selección de Platos";
        $title = "Menú";
        $dishes = Dish::pluck('name', 'id');

        $editar = 0;

        //dd($menu->id);

        //return view('admin.menus.selectDishes', compact('titlePage','title', 'idMenu', 'categories', 'dishes'));
        //return view('admin.menus.selectDishes', compact('titlePage','title', 'menu', 'categories', 'numCategories', 'dishes'));
//        return view('admin.menus.selectDishes',
//            compact('titlePage', 'title', 'menu', 'numCategories', 'selectedCategories', 'dishes'));
//
        return view('admin.menus.selectDishes',    compact('titlePage', 'title', 'menu', 'numCategories', 'selectedCategories', 'dishes', 'editar'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeDish(Request $request)
    {
        //dd($request->all());

        $menu = Menu::find($request->menu_id);

        for ($i = 0; $i < $request->numCategories; $i++) {

            if ($request->has('dishes_' . $request->categories_name[$i])) { //si true

                //dd($request->all($request->name = 'dishes_' . $request->categories_name[$i]));
                $dishes = $request->all($request->name = 'dishes_' . $request->categories_name[$i]);

                //$num_platos = count($dishes);
                $num_platos = count($dishes['dishes_' . $request->categories_name[$i]]);

                //dd('NumPlatos:'.$num_platos);

                for ($j = 0; $j < $num_platos; $j++) {
                    $idDish = $dishes['dishes_' . $request->categories_name[$i]][$j];
                    //dd('IdPlato: '.$idDish);

                    $menu->dishes()->attach($idDish, ['category_id' => $request->categories_id[$i]]);
                }
            }
        }

        return redirect()->route('menus.index')
            ->with('info', 'Menú creado con éxito.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    //public function edit(Integer $idMenu)
    {
        $titlePage = "Edición de Menús";
        $title = "Editar menú";

        $categories = Category::all();
        $types = Type::pluck('name', 'id');

        //si hay dateMenu
        if($menu->dateMenu){
            $dateMenu = date('d/m/Y', strtotime($menu->dateMenu));
        }else{
            $dateMenu = $menu->dateMenu;
        }

        //dd($dateMenu);
        return view('admin.menus.edit', compact('menu', 'titlePage', 'title', 'categories', 'types', 'dateMenu'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'type_id' => 'required',
            'categories' => 'required'
        ]);

        $menu->description = $request->description;
        if($request->price){
            //reemplazamos si hay coma en el precio por .
            $menu->price = str_replace(',', '.', $request->price);
        }

        if($request->dateMenu){
            $menu->dateMenu = Carbon::createFromFormat('d/m/Y', $request->dateMenu);
        }

        $menu->type_id = $request->type_id;
        $menu->update();

        $categories = $request->categories;
        $numCategories = count($categories);

        $selectedCategories = [];
        $data = [];

        for ($i = 0; $i < $numCategories; $i++) {

            $data = DB::table('categories')
                ->select('id', 'name')
                ->where('id', $categories[$i])
                ->first();

            array_push($selectedCategories, $data);
        }

        $titlePage = "Actualización de Platos";
        $title = "Menú";

        //Obtenemos todos los platos por si quiere añadir
        $dishes = Dish::all('name', 'id');

        //Aquí guardaremos los platos del menu y la categoría
        $dishes_menu = [];

        //Obteniendo las categorías asociadas al menu
        foreach ($categories as $category) {

            $dishesDB = DB::table('category_dish_menu')
                ->select('dish_id', 'category_id')
                ->where('menu_id','=', $menu->id)
                ->where('category_id', '=', $category)
                ->get();

            $d = $dishesDB->toArray();

            //Añadimos los platos de la categoria y el menu
            array_push($dishes_menu, $d);
        }

        $editar = 1;

        return view('admin.menus.updateDishes', compact('titlePage', 'title', 'menu', 'numCategories',
            'selectedCategories', 'dishes', 'dishes_menu', 'editar'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function updateDish(Request $request, Menu $menu)
    {
        //dd($request->all());

//        $request->validate([
//            //'type_id' => 'required'
//            //'name' => 'required',
//            //$tipo
//        ]);

        //$menu->update($request->all());
        $menu = Menu::find($request->menu_id);

        for ($i = 0; $i < $request->numCategories; $i++) {

            if ($request->has('dishes_' . $request->categories_name[$i])) { //si true

                //dd($request->all($request->name = 'dishes_' . $request->categories_name[$i]));
                $dishes = $request->all($request->name = 'dishes_' . $request->categories_name[$i]);

                //$num_platos = count($dishes);
                $num_platos = count($dishes['dishes_' . $request->categories_name[$i]]);

                //dd('NumPlatos:'.$num_platos);

                for ($j = 0; $j < $num_platos; $j++) {
                    $idDish = $dishes['dishes_' . $request->categories_name[$i]][$j];
                    //dd('IdPlato: '.$idDish);
                    //dd($request->categories_name[$i]);

                    $dishExist = DB::table('category_dish_menu')
                        ->select('dish_id')
                        ->where('menu_id','=', $menu->id)
                        ->where('category_id', '=', $request->categories_name[$i])
                        ->where('dish_id', '=', $idDish)
                        ->get();

                    //dd($dishExist);
                    //if ($dishExist != $idDish) { //si el plato no es el mismo que hay, añado a BD
                    if(count($dishExist) >= 1) {
                        $menu->dishes()->attach($idDish, ['category_id' => $request->categories_id[$i]]);
                    }

                    //$menu->dishes()->sync($idDish, ['category_id' => $request->categories_id[$i]]);
                }
            }
        }

        return redirect()->route('menus.index')
            ->with('info', 'Menú actualizado con éxito.');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        try {
            $menu->delete();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return redirect()->route('menus.index')
            ->with('info', 'Menú eliminado con éxito.');
    }
}
