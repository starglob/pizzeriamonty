<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*User::create([
            'name' => 'Eva Hervás Puchalt',
            'email' => 'email@email.com',
            'password' => bcrypt('pass1234'),
        ]);*/

        User::create([
            'name' => 'Natalia',
            'email' => 'pizzeriamonty@gmail.com',
            'password' => bcrypt('pMonty2021'),
        ]);
    }
}
