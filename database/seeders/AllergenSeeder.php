<?php

namespace Database\Seeders;

use App\Models\Allergen;
use App\Models\Image;
use Illuminate\Database\Seeder;

class AllergenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allergens = Allergen::factory(15)->create();

        foreach ($allergens as $allergen){
            Image::factory(1)->create([
                'imageable_id' => $allergen->id,
                'imageable_type' => Allergen::class
            ]);
            $allergen->dishes()->attach([
                rand(1, 4)
            ]);
        }
    }
}
