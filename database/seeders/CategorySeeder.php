<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Raciones',
            'description' => '',
        ]);
//
//        Category::create([
//            'name' => 'Carta',
//            'description' => '',
//        ]);

        Category::create([
            'name' => 'Aperitivos',
            //'description' => '',
        ]);

        Category::create([
            'name' => 'Entrantes',
            //'description' => '',
        ]);

        Category::create([
            'name' => 'Principal',
            //'description' => '',
        ]);

        Category::create([
            'name' => 'Carne',
            //'description' => '',
        ]);

        Category::create([
            'name' => 'Pescado',
            //'description' => '',
        ]);

        Category::create([
            'name' => 'Postre',
            //'description' => '',
        ]);
    }
}
