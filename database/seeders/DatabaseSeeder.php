<?php

namespace Database\Seeders;

use App\Models\Dish;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Storage::deleteDirectory('allergens');
        Storage::makeDirectory('allergens');

        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        Dish::factory(5)->create();
        $this->call(AllergenSeeder::class);
    }
}
