<?php

namespace Database\Factories;

use App\Models\Dish;
use Illuminate\Database\Eloquent\Factories\Factory;

class DishFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dish::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //$name = $this->faker->unique()->word(20);
        $ingredients = $this->faker->unique()->word(20);

        return [
            'name' => $this->faker->name,
            'ingredients' => $ingredients,
        ];
    }
}
